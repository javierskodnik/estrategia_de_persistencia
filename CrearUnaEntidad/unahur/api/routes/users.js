var express = require("express");
var router = express.Router();
var models = require("../models");
var jwt = require("jsonwebtoken");
const { token } = require("morgan");



router.post("/",(req, res)=>{
    models.users
    .create({mail: req.body.mail, pass: req.body.pass})
    .then(users => res.status(201).send("Usuario creado correctamente"))
    .catch(error => {
        if (error == "SequelizeUniqueConstraintError: Validation error") {
            res.status(400).send('Bad request: existe otro usuario con el mismo nombre')
        }
        else {
            console.log(`Error al intentar insertar en la base de datos: ${error}`)
            res.sendStatus(500)
        }
        });
    }
);

const findUser = (mail, pass,{ onSuccess, onNotFound, onError }) => {
  models.users
    .findOne({
      attributes: ["id", "mail", "pass"],
      where: { mail, pass },
    })
    .then(users => (users ? onSuccess(users) : onNotFound()))
    .catch(() => onError());
};

router.get("/login", (req, res) => {
    let mail = req.query.mail;
    let pass = req.query.pass;
    findUser(mail,pass, {
      onSuccess: user => jwt.sign({user},'llavesecreta',(err,token)=>{res.send(token)}),
      onNotFound: () => res.sendStatus(404),
      onError: () => res.sendStatus(500)
    });
});

module.exports = router;