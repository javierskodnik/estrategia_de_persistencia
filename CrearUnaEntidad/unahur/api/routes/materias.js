var express = require("express");
var router = express.Router();
var models = require("../models");
var jwt = require("jsonwebtoken");

router.get("/",verificarToken,(req, res) => {
   jwt.verify(req.token,'llavesecreta',(error, authData)=>{
     if(error){
       res.status(400).send("POR AQUI NO ..!!");
     }
     else{
       //agregamos para el paginado
        const paginaActual = parseInt(req.query.paginaActual);
        const cantidadAMostrar = parseInt(req.query.cantidadAMostrar);
        //hasta aca agregamos para el paginado
        console.log("Esto es un mensaje para ver en consola");
        models.materia
          .findAll({
            attributes: ["id","nombre"],
            include:[{as:'Profesores-Relacionados', model:models.profesor, attributes: ["id","nombre"]}],
            //agregamos para el paginado
            offset: (paginaActual - 1) * cantidadAMostrar, 
            //offset:(paginaActual*cantidadAMostrar), cantidadAMostrar,
            limit: cantidadAMostrar  
            //hasta aca agregamos para el paginano
    })
    .then(materias => res.send(materias))
    .catch(() => res.sendStatus(500));
      
     }
   });
 });


 router.post("/",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }  
    else{
      models.materia
    .create({ nombre: req.body.nombre,id_carrera:req.body.id_carrera })
    .then(materia => res.status(201).send({ id: materia.id }))
    .catch(error => {
      if (error == "SequelizeUniqueConstraintError: Validation error") {
        res.status(400).send('Bad request: existe otra materia con el mismo nombre')
      }
      else {
        console.log(`Error al intentar insertar en la base de datos: ${error}`)
        res.sendStatus(500)
      }
    });
    }
  });
});


const findMateria = (id, { onSuccess, onNotFound, onError }) => {
  models.materia
    .findOne({
      attributes: ["id", "nombre"],
      include:[{as:'Profesores-Relacionados', model:models.profesor, attributes:["id","nombre"]}],
      where: { id }
      
    })
    .then(materia => (materia ? onSuccess(materia) : onNotFound()))
    .catch(() => onError());
};

router.get("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }  
    else{
      findMateria(req.params.id, {
        onSuccess: materia => res.send(materia),
        onNotFound: () => res.sendStatus(404),
        onError: () => res.sendStatus(500)
      });
     
    }
  });
});

router.put("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }  
    else{
      const onSuccess = ateria =>
    materia
      .update({ nombre: req.body.nombre }, { fields: ["nombre"] })
      .then(() => res.sendStatus(200))
      .catch(error => {
        if (error == "SequelizeUniqueConstraintError: Validation error") {
          res.status(400).send('Bad request: existe otra materia con el mismo nombre')
        }
        else {
          console.log(`Error al intentar actualizar la base de datos: ${error}`)
          res.sendStatus(500)
        }
      });
    findMateria(req.params.id, {
    onSuccess,
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  }); 
    }
  });
});

router.delete("/:id",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }  
    else{
      const onSuccess = materia =>
    materia
      .destroy()
      .then(() => res.sendStatus(200))
      .catch(() => res.sendStatus(500));
  findMateria(req.params.id, {
    onSuccess,
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  }); 
   }
  });
});


function verificarToken(req, res, run){
  const bearerHeader = req.headers['authorization'];

  if(typeof bearerHeader !== 'undefined' ){
    const bearerToken = bearerHeader.split(" ")[1];
    req.token = bearerToken;
    run();
  }
  else{
    res.sendStatus(403);
  }
}

module.exports = router;
