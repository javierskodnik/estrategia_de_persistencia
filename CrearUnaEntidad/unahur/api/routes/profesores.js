var express = require("express");
var router = express.Router();
var models = require("../models");
var jwt = require("jsonwebtoken");

router.get("/",verificarToken,(req, res) => {
  jwt.verify(req.token,'llavesecreta',(error, authData)=>{
    if(error){
      res.status(400).send("POR AQUI NO ..!!");
    }
    else{
      //agregamos para el paginado
    const paginaActual = parseInt(req.query.paginaActual);
    const cantidadAMostrar = parseInt(req.query.cantidadAMostrar);
    //hasta aca agregamos para el paginado
    console.log("Esto es un mensaje para ver en consola");
    models.profesor
      .findAll({
        attributes: ["id","nombre"],
        include:[{as:'Materia-Relacionada', model:models.materia, attributes: ["id","nombre"]}],
        //agregamos para el paginado
        offset: (paginaActual - 1) * cantidadAMostrar, 
        //offset:(paginaActual*cantidadAMostrar), cantidadAMostrar,
        limit: cantidadAMostrar  
        //hasta aca agregamos para el paginado
      })
      .then(profesores => res.send(profesores))
      .catch(() => res.sendStatus(500));
      
    }
  });
});


router.post("/", (req, res) => {
  models.profesor
    .create({ nombre: req.body.nombre,id_materia:req.body.id_materia })
    .then(profesor => res.status(201).send({ id: profesor.id }))
    .catch(error => {
      if (error == "SequelizeUniqueConstraintError: Validation error") {
        res.status(400).send('Bad request: existe otro profesor con el mismo nombre')
      }
      else {
        console.log(`Error al intentar insertar en la base de datos: ${error}`)
        res.sendStatus(500)
      }
    });
});

const findProfesor = (id, { onSuccess, onNotFound, onError }) => {
  models.profesor
    .findOne({
      attributes: ["id", "nombre"],
      include:[{as:'Materia-Relacionada', model:models.materia, attributes: ["id","nombre"]}],
      where: { id }
      
    })
    .then(profesor => (profesor ? onSuccess(profesor) : onNotFound()))
    .catch(() => onError());
};

router.get("/:id", (req, res) => {
  findProfesor(req.params.id, {
    onSuccess: profesor => res.send(profesor),
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
});

router.put("/:id", (req, res) => {
  const onSuccess = ateria =>
    profesor
      .update({ nombre: req.body.nombre }, { fields: ["nombre"] })
      .then(() => res.sendStatus(200))
      .catch(error => {
        if (error == "SequelizeUniqueConstraintError: Validation error") {
          res.status(400).send('Bad request: existe otra profesor con el mismo nombre')
        }
        else {
          console.log(`Error al intentar actualizar la base de datos: ${error}`)
          res.sendStatus(500)
        }
      });
    findProfesor(req.params.id, {
    onSuccess,
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
});

router.delete("/:id", (req, res) => {
  const onSuccess = profesor =>
    profesor
      .destroy()
      .then(() => res.sendStatus(200))
      .catch(() => res.sendStatus(500));
  findProfesor(req.params.id, {
    onSuccess,
    onNotFound: () => res.sendStatus(404),
    onError: () => res.sendStatus(500)
  });
});

function verificarToken(req, res, run){
  const bearerHeader = req.headers['authorization'];

  if(typeof bearerHeader !== 'undefined' ){
    const bearerToken = bearerHeader.split(" ")[1];
    req.token = bearerToken;
    run();
  }
  else{
    res.sendStatus(403);
  }
}

function verificarToken(req, res, run){
  const bearerHeader = req.headers['authorization'];

  if(typeof bearerHeader !== 'undefined' ){
    const bearerToken = bearerHeader.split(" ")[1];
    req.token = bearerToken;
    run();
  }
  else{
    res.sendStatus(403);
  }
}

module.exports = router;
