'use strict';
module.exports = (sequelize, DataTypes) => {
  const carrera = sequelize.define('carrera', {
    nombre: DataTypes.STRING
  }, {});
  carrera.associate = function(models){
    // associations can be defined here -- la carrera tiene varias materias
    carrera.hasMany(models.materia,{as: 'Materias-Relacionadas', foreignKey: 'id_carrera'})
  };
  
  return carrera;
};