'use strict';
module.exports = (sequelize, DataTypes) => {
  const materia = sequelize.define('materia', {
    nombre: DataTypes.STRING,
    id_carrera: DataTypes.INTEGER
  }, {});
  materia.associate = function(models) {
    // associations can be defined here
    // si relacionamos materia con carrera:
    // materia.belongsTo(models.carrera,{as: 'Carrera-Relacionada', foreignKey: 'id_carrera'})
    //si relacionamos materia con profesor --> la materia tiene varios profesores
    materia.hasMany(models.profesor,{as: 'Profesores-Relacionados', foreignKey: 'id_materia'})
  };
  return materia;
};